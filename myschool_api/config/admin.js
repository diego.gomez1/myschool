module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '14a512f371b996cf07b1343f5e5d3a1b'),
  },
});
