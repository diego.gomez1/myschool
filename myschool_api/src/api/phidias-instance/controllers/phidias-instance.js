"use strict";

/**
 *  phidias-instance controller
 */

const { createCoreController } = require("@strapi/strapi").factories;

module.exports = createCoreController(
  "api::phidias-instance.phidias-instance",
  ({ strapi }) => ({
    async testConnection(ctx) {
      ctx.body = "ok";
    },
  })
);
