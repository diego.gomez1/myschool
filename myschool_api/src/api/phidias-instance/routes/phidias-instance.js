"use strict";

/**
 * phidias-instance router.
 */

const { createCoreRouter } = require("@strapi/strapi").factories;

module.exports = createCoreRouter("api::phidias-instance.phidias-instance");
