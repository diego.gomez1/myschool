module.exports = {
  routes: [
    {
      // Path defined with a URL parameter
      method: "GET",
      path: "/phidias-instance/:id/testConnection",
      handler: "phidias-instance.testConnection",
    },
  ],
};
