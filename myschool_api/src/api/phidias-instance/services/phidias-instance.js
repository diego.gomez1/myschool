"use strict";

/**
 * phidias-instance service.
 */

const { createCoreService } = require("@strapi/strapi").factories;

module.exports = createCoreService(
  "api::phidias-instance.phidias-instance",
  ({ strapi }) => ({
    async testServerConnection(...args) {
      let response = { okay: true };
      return response;
    },
  })
);
