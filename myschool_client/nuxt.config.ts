import { defineNuxtConfig } from "nuxt3";

// https://v3.nuxtjs.org/docs/directory-structure/nuxt.config
export default defineNuxtConfig({
  buildModules: [
    "@nuxtjs/tailwindcss",
    "@nuxtjs/strapi",
    "@formkit/nuxt",
    "@vueuse/nuxt",
  ],
  vueuse: {},
  tailwindcss: {},
  strapi: {
    url: process.env.STRAPI_URL || "http://localhost:1337",
    prefix: "/api",
    version: "v4",
    cookie: {},
  },
  formkit: {
    defaultConfig: true,
    configFile: "./my-configs/formkit.config.ts",
  },
});
