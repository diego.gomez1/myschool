// formkit.config.ts
import { es } from "@formkit/i18n";
import { DefaultConfigOptions } from "@formkit/vue";

const config: DefaultConfigOptions = {
  locales: { es },
  locale: "es",
};

export default {
  config,
};
