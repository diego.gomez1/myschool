import "dotenv/config";
import { program } from "commander";
import { connector as strapiConnector } from "./libs/strapi.mjs";
import { ideodb } from "./libs/phidias.mjs";

program
  .name("myschool_cli")
  .description("CLI tool to work with myschool api")
  .version("0.1.0");

program
  .command("test")
  .description("Test strapi and phidias connections")
  .action(async (str, options) => {
    let strapiConnection = await strapiConnector.testConnection();
    console.log(
      `Stablish connection to Strapi server ${process.env.STRAPI_URL}: ${
        strapiConnection ? "OK" : "FAIL"
      }`
    );

    let dbConnection = await ideodb.testConnection();
    console.log(
      `Stablish connection to DB Ideo: ${dbConnection ? "OK" : "FAIL"}`
    );
  });

program
  .command("update_years")
  .description("Bring years from phidias to strapi")
  .action(async (str, options) => {
    let years = await ideodb.getYears();

    strapiConnector.updateYears(years);
  });

program
  .command("update_teachers")
  .description("Bring teachers from phidias to strapi")
  .action(async (str, options) => {
    let teachers = await ideodb.getActiveTeachers();
    console.log("Got teachers");
    strapiConnector.updateTeachers(teachers);
  });

program.parse();
