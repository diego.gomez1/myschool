import Strapi from "strapi-sdk-js";
import FormData from "form-data";
import axios from "axios";

class StrapiConnector {
  constructor() {
    this.strapi = new Strapi({
      url: process.env.STRAPI_URL || "http://localhost:1337",
      prefix: "/api",
      axiosOptions: {
        headers: {
          Authorization: `bearer ${process.env.STRAPI_API_TOKEN}`,
        },
      },
    });
  }

  async testConnection() {
    try {
      await this.strapi.find("years");
      return true;
    } catch (e) {
      return false;
    }
  }

  async getUsers() {
    try {
      let user = await this.strapi.find("years");
      console.log(user);
      return user;
    } catch (e) {
      console.error(e);
    }

    return null;
  }

  async updateYears(years) {
    await Promise.all(
      years.map(async (year) => {
        let existingYear = await this.strapi.find("years", {
          filters: {
            phidias_id: {
              $eq: year.phidias_id,
            },
          },
        });
        if (existingYear.data.length > 0) {
          console.log("Found", existingYear.data[0].attributes.name, year);
        } else {
          try {
            let newYear = await this.strapi.create("years", year);
            console.log("Created", newYear.name);
          } catch (e) {
            console.error(e);
          }
        }
      })
    );
  }

  async updateTeachers(teachers) {
    for (let teacher of teachers) {
      let existingTeacher = await this.strapi.find("teachers", {
        filters: {
          phidias_id: {
            $eq: teacher.phidias_id,
          },
        },
        populate: ["avatar"],
      });
      if (existingTeacher.data.length > 0) {
        let teacherRecord = existingTeacher.data[0];
        let teacherObj = teacherRecord.attributes;
        console.log("Found", `${teacher.firstname} ${teacher.lastname}`);
        if (teacher.picture_hash && !teacherObj.avatar.data) {
          try {
            const form = new FormData();
            let avatarUrl = `https://phidias-es-storage-1.s3.amazonaws.com/ideo/pictures/current/picture_${teacher.picture_hash}.jpg`;
            const { data: stream } = await axios.get(avatarUrl, {
              responseType: "stream",
            });

            form.append("files", stream);
            form.append("ref", "api::teacher.teacher");
            form.append("refId", teacherRecord.id);
            form.append("field", "avatar");

            await this.strapi.axios.post("/upload", form, {
              headers: form.getHeaders(),
            });
          } catch (e) {
            console.error(e);
          }
        }
      } else {
        try {
          let teacherObj = {
            ...teacher,
          };

          delete teacherObj["group_name"];
          delete teacherObj["groups"];
          delete teacherObj["picture_hash"];

          let newTeacher = await this.strapi.create("teachers", teacherObj);
          console.log("Created", newTeacher.firstname);
        } catch (e) {
          console.error(e);
        }
      }
    }
  }
}

const connector = new StrapiConnector();
console.log("Started");

export { connector };
