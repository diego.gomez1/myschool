import knex from "knex";

function parseDate(seconds) {
  let date = new Date(seconds * 1000);
  let year = `${date.getFullYear()}`;
  let month = `0${date.getUTCMonth() + 1}`.substring(-2);
  let day = `0${date.getDate()}`.substring(-2);

  return `${year}-${month}-${day}`;
}

class PhidiasConnector {
  constructor(instance) {
    console.log("Created instance", instance);
    const phidiasID = `PHIDIAS_DB_${instance.toUpperCase()}`;
    const connectionString = process.env[phidiasID];

    console.log(connectionString, phidiasID);

    this.db = knex({
      client: "mysql2",
      connection: connectionString,
    });
  }

  async testConnection() {
    // console.log("test", this.db.stat);
    if (await this.db.schema.hasTable("sophia_years")) {
      return true;
    } else {
      return false;
    }
  }

  async getYears() {
    if (await this.db.schema.hasTable("sophia_years")) {
      let years = await this.db.select("*").from("sophia_years");

      return years.map((year) => ({
        phidias_id: year.id,
        name: year.name,
        start_period: parseDate(year.start_date),
        end_period: parseDate(year.end_date),
      }));
    }

    return null;
  }

  async getCurrentYear() {
    let today = new Date();
    let todaySeconds = Math.floor(today.getTime() / 1000);
    let years = await this.db
      .select("*")
      .from("sophia_years")
      .where("sophia_years.start_date", "<", todaySeconds)
      .andWhere("sophia_years.end_date", ">", todaySeconds);

    return years[0];
  }

  async getActiveTeachers() {
    let currentYear = await this.getCurrentYear();

    let employees = await this.db
      .from("sophia_person_employee_group_memberships as m")
      .where("m.year", "=", currentYear.id)
      .join("sophia_person_employee_groups as g", "g.id", "m.group")
      .rightJoin("sophia_people as p", "p.id", "m.person")
      .columns([
        { phidias_id: "p.id" },
        "p.email",
        "p.firstname",
        "p.lastname",
        "p.document",
        "p.picture_hash",
        "p.phone",
        "m.status",
        "p.mobile",
        { group_name: "g.name" },
      ]);

    let employeesById = {};

    employees.map((e) => {
      employeesById[e.phidias_id] = employeesById[e.phidias_id] || {
        ...e,
        groups: [],
      };

      employeesById[e.phidias_id].groups.push(e.group_name);
    });

    return Object.values(employeesById);
  }
}

const ideodb = new PhidiasConnector("ideo");
// const ciclosdb = new PhidiasConnector("ciclos");

export { ideodb };
